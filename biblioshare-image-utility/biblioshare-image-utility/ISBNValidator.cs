﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImageUtility
{
    public static class ISBNValidator
    {
        public static bool ValidateISBN(string strISBN)
        {
            if (String.IsNullOrEmpty(strISBN))
            {
                return false;
            }
            else if (strISBN.Length == 10)
            {
                return (strISBN.Substring(0, 9).All(c => Char.IsNumber(c)) && strISBN[9] == CalculateCheckDigit10(strISBN));
            }
            else if (strISBN.Length == 12)
            {
                return (strISBN.Substring(0, 12).All(c => Char.IsNumber(c)) && ((strISBN.Substring(0, 3) == "978") || (strISBN.Substring(0, 3) == "979")));
            }
            else if (strISBN.Length == 13)
            {
                return ((strISBN.Substring(0, 3) == "978") || (strISBN.Substring(0, 3) == "979")) &&
                       (strISBN.Substring(0, 13).All(c => Char.IsNumber(c))) &&
                       (strISBN[12] == CalculateCheckDigit13(strISBN));
            }
            else
            {
                return false;
            }
        }

        public static char CalculateCheckDigit13(string strISBN)
        {
            if ((strISBN == null) || (strISBN.Length < 12))
            {
                throw new ArgumentException("Argument must be a string of at least 12 characters");
            }

            int intSUM;
            intSUM = (Convert.ToInt16(strISBN.Substring(0, 1)) * 1) +
                        (Convert.ToInt16(strISBN.Substring(1, 1)) * 3) +
                        (Convert.ToInt16(strISBN.Substring(2, 1)) * 1) +
                        (Convert.ToInt16(strISBN.Substring(3, 1)) * 3) +
                        (Convert.ToInt16(strISBN.Substring(4, 1)) * 1) +
                        (Convert.ToInt16(strISBN.Substring(5, 1)) * 3) +
                        (Convert.ToInt16(strISBN.Substring(6, 1)) * 1) +
                        (Convert.ToInt16(strISBN.Substring(7, 1)) * 3) +
                        (Convert.ToInt16(strISBN.Substring(8, 1)) * 1) +
                        (Convert.ToInt16(strISBN.Substring(9, 1)) * 3) +
                        (Convert.ToInt16(strISBN.Substring(10, 1)) * 1) +
                        (Convert.ToInt16(strISBN.Substring(11, 1)) * 3);

            int intMOD;
            intMOD = 10 - (intSUM % 10);

            return (char)('0' + (intMOD == 10 ? 0 : intMOD));
        }

        public static char CalculateCheckDigit10(string strISBN)
        {
            if ((strISBN == null) || (strISBN.Length < 9))
            {
                throw new ArgumentException("Argument must be a string of at least 9 characters");
            }

            int intSUM;
            intSUM = (Convert.ToInt16(strISBN.Substring(0, 1)) * 1) +
                        (Convert.ToInt16(strISBN.Substring(1, 1)) * 2) +
                        (Convert.ToInt16(strISBN.Substring(2, 1)) * 3) +
                        (Convert.ToInt16(strISBN.Substring(3, 1)) * 4) +
                        (Convert.ToInt16(strISBN.Substring(4, 1)) * 5) +
                        (Convert.ToInt16(strISBN.Substring(5, 1)) * 6) +
                        (Convert.ToInt16(strISBN.Substring(6, 1)) * 7) +
                        (Convert.ToInt16(strISBN.Substring(7, 1)) * 8) +
                        (Convert.ToInt16(strISBN.Substring(8, 1)) * 9);

            int intMOD;
            intMOD = (intSUM % 11);

            return (intMOD == 10 ? 'X' : (char)('0' + intMOD));

        }
    }
}