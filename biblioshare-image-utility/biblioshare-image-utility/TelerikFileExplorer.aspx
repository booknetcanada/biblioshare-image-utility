﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TelerikFileExplorer.aspx.cs" Inherits="ImageUtility.TelerikFileExplorer" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI, Version=2019.2.514.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="stylesheets/style.css" rel="stylesheet" />

    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <telerik:RadScriptManager runat="server"></telerik:RadScriptManager>
            <telerik:RadFileExplorer ID="RadFileExplorer2" EnableFilterTextBox="True" EnableOpenFile="False" EnableFilteringOnEnterPressed="True" runat="server" ExplorerMode="Thumbnails" CssClass="rfeLargeThumbnails" OnClientLoad="ClientLoad"
                                     DisplayUpFolderItem="false" EnableCreateNewFolder="false">
            </telerik:RadFileExplorer>
        </div>
    </form>
<script>
    function ClientLoad(explorer, args) {
        explorer.get_fileList().get_grid().add_rowDragStarted(RowDragStarted)
    }
    function RowDragStarted(grid, args) {
        args.set_cancel(true);
    }
</script>

</body>
</html>
