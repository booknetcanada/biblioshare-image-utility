﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImageUtility.aspx.cs" Inherits="ImageUtility.ImageUtility" MaintainScrollPositionOnPostback="True" EnableEventValidation="false" SmartNavigation="True" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI, Version=2019.2.514.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>
<%@ Register TagPrefix="a" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI, Version=2019.2.514.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Image Utility</title>
    <link href="stylesheets/style.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            var sPositions = localStorage.positions || "{}", positions = JSON.parse(sPositions);
                $.each(positions, function(id, pos) {
                    $("#" + id).css(pos);
                });
            $("#draggable").draggable({
                containment: "#containment-wrapper",
                scroll: false,
                stop: function (event, ui) {
                    positions[this.id] = ui.position;
                    localStorage.positions = JSON.stringify(positions);
                }
            });
        });
    </script>
</head>
<body runat="server">
<form id="form1" runat="server">
    <div id ="draggable" runat="server" Visible="False">
        <asp:Button ID ="btnCloseExplorerPanel" CssClass="btnCloseExplorer" runat="server" OnClick="btnCloseExplorerPanel_OnClick"   Text="x"></asp:Button>
        <div style="position: absolute; z-index: 1;">
            <telerik:RadScriptManager runat="server" EnablePageMethods="True" EnablePartialRendering="True"></telerik:RadScriptManager>
            <telerik:RadFileExplorer ID="RadFileExplorer1" Width="800px" EnableFilterTextBox="True" EnableOpenFile="False"  AllowPaging="True" PageSize="15"
                                     EnableFilteringOnEnterPressed="True" runat="server" OnClientItemSelected="onClientItemSelected"
                                     DisplayUpFolderItem="false" Font="Arial" CssClass="rfeLargeThumbnails" RenderMode="Lightweight" EnableCreateNewFolder="false">
            </telerik:RadFileExplorer>
        </div>
        <div id="divPreviewImage" runat="server">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                <asp:Button runat="server" ID="btnPreview" CssClass="btnPreview" Text="Preview Image:" OnClick="btnPreview_OnClick"/>
                <asp:Image runat="server" CssClass="PreviewImage" ClientIDMode="Static" ID="PreviewImage" ImageUrl="ImageUtility.aspx?PreviewPath="/>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnPreview" />

                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <div style="width:1600px; height: 340px;">
        <div style="float: left; width: 250px; height:340px;">
            <asp:Label ID="lblISBNSearch" Font-Bold="True" runat="server" Text="Please enter valid ISBN(s):"></asp:Label> 
            <br/>
            <asp:TextBox ID="txtISBNSearch" runat="server" style="resize:none;" Height="340px" Width="250px" TextMode="MultiLine" Wrap="True"></asp:TextBox>
            </div>
        <div style="float: left; width: 250px; height:340px;">
            <asp:Label ID="lblISBNMissing" Visible="False" Font-Bold="True" ForeColor="Red" runat="server" Text="ISBN(s) not found: "></asp:Label> 
            <br/>
            <asp:TextBox ID="txtISBNMissing" runat="server" Visible="False" style="resize:none;" ReadOnly="True" Height="340px" Width="250px" TextMode="MultiLine" Wrap="True"></asp:TextBox>
        </div>
        <div style="float: left; width: 250px;height: 340px;">
            <asp:Label ID="lblISBNDeleted" Visible="False" Font-Bold="True" ForeColor="Green" runat="server" Text="Image(s) deleted for ISBN(s): "></asp:Label> 
            <br/>
            <asp:TextBox ID="txtISBNDeleted" runat="server" Visible="False" style="resize:none;" ReadOnly="True" Height="340px" Width="250px" TextMode="MultiLine" Wrap="True"></asp:TextBox>
        </div>
        <br/>
        <br/>
    </div>
    <br/>
    <br/>
            
    <div>            
        <asp:Button ID="btnUpdateList" CssClass="generalButton" runat="server" Text="Update List" OnClick="btnUpdateList_OnClick" Width="100px"/>
    </div>
    <asp:Label ID="lblErrorLabel" ForeColor="Red" Font-Bold="True" Width="600px" runat="server" Visible="False"/>
    <br/>
    <asp:GridView ID="gvAccountGrid" CssClass="Grid"  runat="server" Width="600px" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="SupplierName" HeaderText="Supplier"/>
            <asp:BoundField DataField="NumImage" HeaderText="Number of images for this account" />
        </Columns>
        <HeaderStyle CssClass="HeaderStyle" />
        <RowStyle CssClass="RowStyle" />
        <AlternatingRowStyle CssClass="AlternateRowStyle" />
    </asp:GridView>

    <div class="gridControls">
        <asp:Button ID="btnDelete" CssClass="generalButton" runat="server" Text="Delete" Width="100px" OnClick="btnDelete_OnClick" Visible="False" />
        <asp:Button ID="btnViewFolder" CssClass="generalButton"  runat="server" Text="View Folder(s)" Width="100px" OnClick="btnViewFolder_OnClick" Visible="False"/>
        <asp:CheckBox ID="chkShowTestData" Checked="True" Visible="False" CssClass="ShowTestData" Text="Show Test Data" OnCheckedChanged="chkShowTestData_OnCheckedChanged" AutoPostBack="true" runat="server"/>
    </div>

    <br/>

    <div id ="EditPanel" runat="server" Visible="False"  style="position: absolute; background-color: whitesmoke; width: 900px; height: 430px;  border-width: 1px; border-color: lightgray;">
        <asp:Button ID ="btnCloseEditPanel" runat="server" CssClass="btnClosePanel" OnClick="btnCloseEditPanel_OnClick"  Text="x"></asp:Button>
        <div>
            <asp:Label runat="server" ID="lblPanelISBN" Font-Bold="True" Text="EAN: "></asp:Label>
            <asp:Label runat="server" ID="txtPanelISBN" Text="$ISBN"></asp:Label>
        </div>

        <div>  
            <asp:Label runat="server" ID="lblFileFullPath" Font-Bold="True" Text="File Path: "></asp:Label>
            <asp:Label runat="server" ID="txtPanelFilePath" Text="$FULL_FILE_PATH"></asp:Label>
        </div> 
        <br/>

        <div class="LeftPanel">   
            <asp:Image runat="server" ID="CurrentImage" ImageUrl ="ImageUtility.aspx?FilePath="></asp:Image>
            <div>
                <asp:Button ID = "btnImageDelete" CssClass="generalButton" runat="server" Text ="Delete Image" OnClick="btnImageDelete_OnClick"></asp:Button>
                <asp:Button ID = "btnImageViewFolder" CssClass="generalButton" runat="server" Text ="View Folder" OnClick="btnImageViewFolder_OnClick"></asp:Button>
            </div>
        </div>
        
        <div class ="RightPanel">
            <asp:Panel ID="ReplacePanel" Visible="True" runat="server" BorderStyle="None">
                <asp:Image ID="NewImage" ImageUrl="" runat="server" Visible="True"/>
                <div>
                    <asp:FileUpload runat="server" CssClass="FileUpload" ID="FileUpload1" ClientIDMode="Static" onchange="this.form.submit()" AllowMultiple="False" accept=".png,.jpg,.jpeg,.gif"/>
                </div>
                <br/>
                <div>
                    <asp:Label runat="server" ID="lblSave" Visible="False" Font-Size="12px" Font-Italic="True" Font-Bold="True" Text="This will also change the thumbnail for this image."/>
                    <br/>
                    <asp:Button runat="server"  ID="btnSave" CssClass="generalButton" Text="Save Changes" Visible="False" OnClick="btnSave_OnClick"></asp:Button>
                </div>
            </asp:Panel>
        </div>

    </div>

    <div id="divResults" style="width: 1600px; height:400px; overflow: scroll;" runat="server" Visible="False" onscroll="SetDivPosition()">
        <asp:GridView ID="gvResultsGrid" CssClass="Grid"  runat="server" ClientIDMode="Static" Width="1580px" HeaderStyle="398px;" AutoGenerateColumns="False" CellPadding="10" CellSpacing="10">
            <Columns>
                <asp:templatefield HeaderText="Select">
                    <HeaderTemplate>
                        <asp:checkBox ID="chkAll" runat="server" Text="Select/Deselect All" OnCheckedChanged="chkAll_CheckedChanged" AutoPostBack="true" />
                    </HeaderTemplate>
                    <itemtemplate>
                        <asp:checkbox ID="cbSelect" CssClass="gridCB" runat="server"></asp:checkbox>
                    </itemtemplate>
                </asp:templatefield>
                    
                <asp:templatefield HeaderText="Edit Record">
                    <itemtemplate>
                        <asp:ImageButton id="btnEdit" runat="server" AlternateText="Edit Button" ImageUrl="images/EditButton.png" Width="20px" Height="20px" OnClick="btnEdit_OnClick"/>
                    </itemtemplate>
                </asp:templatefield>

                <asp:BoundField DataField="EAN" HeaderText="EAN"/>
                <asp:BoundField DataField="SupplierName" HeaderText="Company"/>
                <asp:BoundField DataField="Perspective" HeaderText="Perspective"/>
                <asp:BoundField DataField="DateProcessed" HeaderText="Date Processed"/>
                <asp:BoundField DataField="FileNumber" HeaderText="File Number" />
                <asp:BoundField DataField="FullFilePath" HeaderText="Full File Path"/>
            </Columns>
            <HeaderStyle CssClass="HeaderStyle" />
            <RowStyle CssClass="RowStyle"/>
            <AlternatingRowStyle CssClass="AlternateRowStyle" />
        </asp:GridView>
        <input type="hidden" id="div_position" name="div_position" />
    </div>
    <asp:HiddenField ID="hdnfldVariable" runat="server"/>
</form>

    <script>

        var theForm = document.forms['form1'];

        if (!theForm)
        {
            theForm = document.form1;
        }

        function __doPostBack(eventTarget, eventArgument)
        {
            if (!theForm.onsubmit || (theForm.onsubmit() != false))
            {
                theForm.__EVENTTARGET.value = eventTarget;
                theForm.__EVENTARGUMENT.value = eventArgument;
                theForm.submit();
            }
        }

        function toolbarClicked(toolbar, args) {
            var buttonValue = args.get_item().get_value();
            localStorage.setItem("theClickedButton", buttonValue);
        }


        
      
        function onClientItemSelected(fileExplorer, args) {
            if (args.get_item().get_type() == Telerik.Web.UI.FileExplorerItemType.File) {
          
                var physicalPath = args.get_item().get_path();

                document.getElementById("<%=hdnfldVariable.ClientID%>").value = physicalPath;

                var btn = document.getElementById("<%=btnPreview.ClientID%>");
                if (btn) btn.click();
            }
          
        }

        window.onload = function () {
            var h = document.getElementById('div_position');
            document.getElementById("divResults").scrollTop = h.value;
        }
        function SetDivPosition() {
            var intY = document.getElementById("divResults").scrollTop;
            var h = document.getElementById('div_position');
            h.value = intY;
        }

        window.onload = MoveDivPos();

        function MoveDivPos() {

            var div = document.getElementById("divResults");
            var div_position = document.getElementById("div_position");
            var position = parseInt('<%=Request.Form["div_position"] %>');
            if (isNaN(position)) {
                try {
                    position = '<%=ImageUtility.DB.ResultGridDivPosition %>';
                }
                catch (err) {
                    position = 0;
                }
            }

            div.scrollTop = position;
            div.onscroll = function () {
                div_position.value = div.scrollTop;
            };
        }

        function ImageEditor_OnClientSaved(imgEditor, args) {
            fileExplorer.clearFolderCache(); //clear the folder cache to force RadFileExplorer repopulate the folders
            fileExplorer.refresh(args.get_fileName()); //refresh the explorer to display newly saved image
        }


        function ClientLoad(explorer, args) {
            explorer.get_fileList().get_grid().add_rowDragStarted(RowDragStarted);
            var tree = explorer.get_tree();//get reference to the TreeView
            tree.add_nodePopulated(nodePopulated);//attach ClientNodePopulated handler
            expandSubNodes(tree);//manually expand initially visible nodes
        }
        function RowDragStarted(grid, args) {
            args.set_cancel(true);
        }

        function nodePopulated(sender, args) {
            var currentNode = args.get_node();//get reference to the expanded node
            expandSubNodes(currentNode);
        }
        function expandSubNodes(parent) {
            var nodes = parent.get_allNodes();//get an array of all sub-nodes
            for (var i = 0; i < nodes.length; i++) {
                nodes[i].expand();
            }
        }

        function OnClientFolderChange(explorer, args)
        {
            explorer.clearFolderCache();
        }

    </script>
</body>
</html>


        