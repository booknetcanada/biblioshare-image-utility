﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ImageUtility.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="stylesheets/style.css" rel="stylesheet"/>
    <title></title>
</head>
<body>
<div class ="loginContainer">
    <asp:Label ID ="lblError" ForeColor="red" runat="server"></asp:Label>

    <form id="form1" runat="server">
        <table>
            <tr>
                <td style="text-align:right">Username/Email:</td>
                <td style="text-align:left"><asp:TextBox ID="txtEmailAddress" runat="server"/></td>
            </tr>
            <tr>
                <td style="text-align:right">Password:</td>
                <td style="text-align:left"><asp:TextBox ID="txtPassword" runat="server" TextMode="Password"/></td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Button ID="btnLogin" runat="server" CssClass="generalButton" Text="Login" OnClick="btnLogin_OnClick"/></td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>
