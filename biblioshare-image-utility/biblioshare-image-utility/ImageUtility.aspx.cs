﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.FileExplorer;
using Image = System.Web.UI.WebControls.Image;

namespace ImageUtility
{
    public partial class ImageUtility : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["BiblioShare"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

            lblErrorLabel.Text = "";
            lblErrorLabel.Visible = false;
            Page.MaintainScrollPositionOnPostBack = true;

            if (!Authenticated.LoggedIn)
            {
                Response.RedirectPermanent("Login.aspx");
            }

            if (Request.QueryString["PreviewPath"] != null)
            {
                string filePath = Request.QueryString["PreviewPath"];
                if (!File.Exists(filePath))
                {
                    lblErrorLabel.Visible = true;
                    lblErrorLabel.Text = lblErrorLabel.Text + "This image exists in the database but not in the FTP folder!";
                    return;
                }

                try
                {
                    string filename = "";
                    // Read the file and convert it to Byte Array
                    int index = filePath.LastIndexOf("\\");

                    if (index > 0)
                    {
                        filename = filePath.Substring(index + 1);
                    }
                    else
                    {
                        filename = filePath;
                    }

                    string contenttype = "image/" + Path.GetExtension(filePath).Replace(".", "");
                    FileStream fs = new FileStream(filePath,
                    FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    br.Close();
                    fs.Close();

                    //Write the file to response Stream
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = contenttype;
                    Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                    Response.BinaryWrite(bytes);
                    Response.Flush();
                    Response.End();
                }
                catch (Exception t)
                {
                    lblErrorLabel.Visible = true;
                    lblErrorLabel.Text = t.Message;
                    Console.WriteLine(t.Message);
                }
            }

            //if (!Page.IsPostBack)
            //{
                //RadFileExplorer1.ExplorerMode = DB.ExplorerMode;

                RadFileExplorer1.Configuration.ViewPaths = DB.ViewPaths;
                RadFileExplorer1.Configuration.SearchPatterns = new[] {"*.*"};
                RadFileExplorer1.Configuration.ContentProviderTypeName =
                    typeof(CustomFileSystemProvider).AssemblyQualifiedName;

            // }
           

            if (IsPostBack && FileUpload1.PostedFile != null && !String.IsNullOrEmpty(FileUpload1.FileName))
            {
                lblSave.Visible = true;
                btnSave.Visible = true;
                string folderPath = Server.MapPath("~/Files/");

                //Check whether Directory (Folder) exists.
                if (!Directory.Exists(folderPath))
                {
                    //If Directory (Folder) does not exists Create it.
                    Directory.CreateDirectory(folderPath);
                }

                //Save the File to the Directory (Folder).
                FileUpload1.SaveAs(folderPath + Path.GetFileName(FileUpload1.FileName));

                //Display the Picture in Image control.
                NewImage.ImageUrl = "~/Files/" + Path.GetFileName(FileUpload1.FileName);
                Size sizethumb = new Size(180, 180);
                // System.Drawing.Image image = System.Drawing.Image.FromFile(folderPath + FileUpload1.FileName);
                // thumbnailForImage = ImageUtility.Resize(image, sizethumb);
                NewImage.Width = 180;
                NewImage.Height = 260;
                NewImage.Visible = true;
            }

            if (Request.QueryString["FilePath"] != null)
            {
                string filePath = Request.QueryString["FilePath"];
                if (!File.Exists(filePath))
                {
                    lblErrorLabel.Visible = true;
                    lblErrorLabel.Text = lblErrorLabel.Text + "This image exists in the database but not in the FTP folder!";
                    return;
                }

                try
                {
                    string filename = "";
                    // Read the file and convert it to Byte Array
                    int index = filePath.LastIndexOf("\\");

                    if (index > 0)
                    {
                        filename = filePath.Substring(index + 1);
                    }
                    else
                    {
                        filename = filePath;
                    }

                    string contenttype = "image/" + Path.GetExtension(filePath).Replace(".", "");
                    FileStream fs = new FileStream(filePath,
                    FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    Byte[] bytes = br.ReadBytes((Int32) fs.Length);
                    br.Close();
                    fs.Close();    

                    //Write the file to response Stream
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = contenttype;
                    Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                    Response.BinaryWrite(bytes);
                    Response.Flush();
                    Response.End();
                }
                catch (Exception t)
                {
                    lblErrorLabel.Visible = true;
                    lblErrorLabel.Text = t.Message;
                    Console.WriteLine(t.Message);
                }
            }
        }

        protected void chkAll_CheckedChanged(object sender, EventArgs e)
        {
            bool allChecked = true;

            //loop through checkboxes to see if they are all checked
            foreach (GridViewRow item in gvResultsGrid.Rows)
            {
                if ((item.Cells[0].FindControl("cbSelect") as CheckBox).Checked == false)
                {
                    allChecked = false;
                    break;
                }
            }

            //if they are not all checked
            if (allChecked == false)
            {
                //if only some checkboxes are checked, set all to checked
                foreach (GridViewRow item in gvResultsGrid.Rows)
                {
                    (item.Cells[0].FindControl("cbSelect") as CheckBox).Checked = true;
                }
            }
            //if they are all checked, should set all to unchecked
            else
            {
                foreach (GridViewRow item in gvResultsGrid.Rows)
                {
                    (item.Cells[0].FindControl("cbSelect") as CheckBox).Checked = false;
                }
            }
        }
        protected void btnViewFolder_OnClick(object sender, EventArgs e)
        {
            int checkedCount = ResultsGridCheckCount();
            if (checkedCount > 1)
            {
                lblErrorLabel.Visible = true;
                lblErrorLabel.Text = "You can only view the folder of one item at a time. Make sure you have just one item selected!";
                return;
            }
            bool atLeastOneChecked = false;
            foreach (GridViewRow item in gvResultsGrid.Rows)
            {
                if ((item.Cells[0].FindControl("cbSelect") as CheckBox).Checked)
                {
                    string filePath = item.Cells[7].Text;
                    string subPath = "";
                    try
                    {
                        string perspective = item.Cells[4].Text;
                        if (perspective.ToLower() != "cover")
                        {
                            lblErrorLabel.Visible = true;
                            lblErrorLabel.Text = "You cannot edit images that are not cover images!";
                            return;
                        }

                        atLeastOneChecked = true;
                        int index = item.Cells[7].Text.LastIndexOf("\\");
                        if (index > 0)
                        {
                            subPath = item.Cells[7].Text.Substring(0, index);
                            subPath = subPath.Replace(@"\\", @"\");

                            var fileCount = (from file in Directory.EnumerateFiles(@subPath, "*", SearchOption.AllDirectories)
                                select file).Count();

                            long numOfFiles = 0;
                            numOfFiles = Convert.ToInt64(fileCount);

                            if (numOfFiles > 20000) //loads about 2500 files per seconds. this 20000 limit means the user should wait no more than 8 seconds
                            {
                                lblErrorLabel.Visible = true;
                                lblErrorLabel.Text =
                                    "The view folder feature is disabled for folders that contain over 20000 files. You can still view the current image for this ISBN by using the Edit Button.";
                                return;
                            }

                            DB.ViewPaths[0] = @subPath;
                            draggable.Visible = true;
                            PreviewImage.ImageUrl = "ImageUtility.aspx?PreviewPath=" + filePath;
                            PreviewImage.Width = 180;
                            PreviewImage.Height = 260;
                            Page_Load(null, EventArgs.Empty);
                        }
                        else if (index == -1)
                        {
                            index = item.Cells[7].Text.LastIndexOf("/");
                            if (index > 0)
                            {
                                subPath = item.Cells[7].Text.Substring(0, index);
                                subPath = subPath.Replace(@"/", @"\");

                                var fileCount = (from file in Directory.EnumerateFiles(@subPath, "*", SearchOption.AllDirectories)
                                    select file).Count();

                                long numOfFiles = 0;
                                numOfFiles = Convert.ToInt64(fileCount);

                                if (numOfFiles > 20000)
                                {
                                    lblErrorLabel.Visible = true;
                                    lblErrorLabel.Text =
                                        "The view folder feature is disabled for folders that contain over 20000 files. You can still view the current image for this ISBN by using the Edit Button.";
                                    return;
                                }

                                DB.ViewPaths[0] = @subPath;
                                draggable.Visible = true;

                                PreviewImage.ImageUrl = "ImageUtility.aspx?PreviewPath=" + filePath;
                                PreviewImage.Width = 180;
                                PreviewImage.Height = 260;
                                Page_Load(null, EventArgs.Empty);
                                // Process.Start(subPath);
                            }
                        }
                    }
                    catch (Exception pathException)
                    {
                        lblErrorLabel.Visible = true;
                        lblErrorLabel.Text = "Process cancelled, cannot find the path: " + subPath + "pathException: " + pathException.StackTrace;
                        return;
                    }
                }
            }

            if (!atLeastOneChecked)
            {
                lblErrorLabel.Visible = true;
                lblErrorLabel.Text = "Please select the record(s) you are trying to view!";
            }
        }

        private int ResultsGridCheckCount()
        {
            int count = 0;
            foreach (GridViewRow item in gvResultsGrid.Rows)
            {
                if ((item.Cells[0].FindControl("cbSelect") as CheckBox).Checked)
                {
                    count++;
                }
            }
            return count;
        }

        protected void btnCloseEditPanel_OnClick(object sender, EventArgs e)
        {
            EditPanel.Visible = false;
            gvAccountGrid.Visible = true;
            divResults.Visible = true;
            gvAccountGrid.Visible = true;
            btnDelete.Visible = true;
            btnViewFolder.Visible = true;
            chkShowTestData.Visible = true;
            NewImage.ImageUrl = null;
            NewImage.Dispose();
            NewImage = null;
            FileUpload1.Dispose();
        }

        protected void btnCloseExplorerPanel_OnClick(object sender, EventArgs e)
        {
            draggable.Visible = false;
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            lblErrorLabel.Visible = false;
            string ISBNSToDelete = "";
            foreach (GridViewRow item in gvResultsGrid.Rows)
            {
                if ((item.Cells[0].FindControl("cbSelect") as CheckBox).Checked)
                {
                    string perspective = item.Cells[4].Text;
                    if (perspective.ToLower() != "cover")
                    {
                        lblErrorLabel.Visible = true;
                        lblErrorLabel.Text = "You cannot edit images that are not cover images!";
                        return;
                    }

                    string isbn = item.Cells[2].Text;
                    string fullFilePath = item.Cells[7].Text;
                    if (File.Exists(fullFilePath))
                    {
                        var fileLength = new FileInfo(fullFilePath).Length;
                        string loginID = GetLoginIDFromFilePath(fullFilePath);
                        if (loginID == "") return;

                        DeleteImageAndThumbnail(fullFilePath);
                        WriteISBNChanges(isbn, txtISBNSearch, txtISBNDeleted);


                        //Delete DB Reference
                        try
                        {
                            DB.DeleteFromDB(isbn, loginID, "cover");
                        }
                        catch (Exception dbCallException)
                        {
                            lblErrorLabel.Visible = true;
                            lblErrorLabel.Text = "DELETE ERROR " + dbCallException.StackTrace;
                            return;
                        }


                        LogItemModel logItem = new LogItemModel();
                        logItem.LoginID = loginID;
                        logItem.IncomingPath = fullFilePath;
                        logItem.OutgoingPath = "DELETED";
                        logItem.FileSize = fileLength;
                        logItem.ProcessStateID = 4;
                        logItem.FileTypeID = 1;
                        logItem.DateUploaded = DateTime.Now;
                        logItem.DateCompleted = DateTime.Now;
                        logItem.TraceInfo = "ImageUtility - Deleting file at " + fullFilePath;
                        logItem.ErrorInfo = "";
                        try
                        {
                            DB.LogChange(logItem);
                        }
                        catch (Exception dbCallException)
                        {
                            lblErrorLabel.Visible = true;
                            lblErrorLabel.Text = "LOG ERROR " + dbCallException.StackTrace;
                            return;
                        }


                        lblISBNDeleted.Visible = true;
                        txtISBNDeleted.Visible = true;
                    }
                    else
                    {
                        lblErrorLabel.Visible = true;
                        lblErrorLabel.Text = lblErrorLabel.Text + item.Cells[2].Text + ": " + "File not found in local folder!" + Environment.NewLine;
                    }
                }
            }

            if (!String.IsNullOrEmpty(txtISBNDeleted.Text))
            {
                txtISBNDeleted.Visible = true;
                lblISBNDeleted.Visible = true;
            }
            else if(lblErrorLabel.Visible == false)
            {
                lblErrorLabel.Visible = true;
                lblErrorLabel.Text = "Please try and select the records you are trying to delete!" + Environment.NewLine;
            }

            LoadGrids(chkShowTestData.Checked);
        }

        //Gets LoginID from FullFilePath eg. for given path: e:\\BNCImages\\1150081\\9780888682567.jpg -> returns 1150081
        private string GetLoginIDFromFilePath(string fullFilePath)
        {
            string loginID = "";
            if (fullFilePath.Contains("FromMediaLink"))
                return loginID = "FromMediaLink";


            try
            {
                loginID = System.IO.Path.GetDirectoryName(fullFilePath);

                int index = loginID.LastIndexOf("\\");
                if (index > 0)
                {
                    index = loginID.LastIndexOf("\\");
                    loginID = loginID.Substring(index);
                    loginID = loginID.Replace("\\", "");
                }
                else if (index == -1)
                {
                    index = loginID.LastIndexOf("/");
                    if (index > 0)
                    {
                        index = loginID.LastIndexOf("/");
                        loginID = loginID.Substring(index);
                        loginID = loginID.Replace("/", "");
                    }
                }
            }
            catch (Exception e)
            {
                lblErrorLabel.Visible = true;
                lblErrorLabel.Text = "Error processing this file path: " + fullFilePath + "! Please contact support for this issue." + "EXCEPTION: " + e.Message;
                return loginID = "";
            }

            return loginID;
        }

        //Deletes image file and associated thumbnail from ftp
        private void DeleteImageAndThumbnail(string fullFilePath)
        {
            File.Delete(fullFilePath);
            int periodPosition = fullFilePath.LastIndexOf('.');
            string ThumbnailFile = fullFilePath.Insert(periodPosition, "_th");
            File.Delete(ThumbnailFile);
        }

        //Sets Visibility to false for all controls on bottom half of screen (everything below Update List button)
        public void SetBottomHalfVisibility(bool state)
        {
            btnDelete.Visible = state;
            btnViewFolder.Visible = state;
            chkShowTestData.Visible = state;
            //ResultGridContainer.Visible = state;
            divResults.Visible = state;
            //gvResultsGrid.Visible = state;
            gvAccountGrid.Visible = state;
        }

        //Clear gridview data
        public void ClearGridData(GridView gridToClear)
        {
            gridToClear.DataSource = null;
            gridToClear.DataBind();
        }

        protected void btnUpdateList_OnClick(object sender, EventArgs e)
        {
            CurrentImage.ImageUrl = "images/NoImageFound.png";
            NewImage.ImageUrl = "images/NoImageSelected.png;";
            PreviewImage.ImageUrl = "";
            ClearTempImageFiles();
            EditPanel.Visible = false;
            LoadGrids(chkShowTestData.Checked);
        }

        private void LoadGrids(bool showTestData)
        {
            bool validISBNs;
            string ISBNList = "";
            string s = txtISBNSearch.Text;
            ClearAndHideTextBox(txtISBNMissing);
            lblISBNMissing.Visible = false;
            if (!String.IsNullOrEmpty(s))
            {
                s = RemoveNewLinesAndTabs(s);
                if (s.Contains(","))
                {
                    ISBNList = ProcessListOfISBNs(s);
                    if (ISBNList == "Error")
                        return;
                }
                else
                {
                    ISBNList = ProcessSingleISBN(s);
                    if (ISBNList == "Error")
                        return;
                }

                DataTable dataTable = DB.GetResultsDataTable(ISBNList, showTestData);
                gvResultsGrid.DataSource = dataTable;
                gvResultsGrid.DataBind();
                
                SetBottomHalfVisibility(true);

                string noSingleQuoteISBNList = ISBNList.Replace("'", "");
                ShowISBNsNotFoundInDatabase(noSingleQuoteISBNList);

                if (!String.IsNullOrEmpty(txtISBNMissing.Text))
                {
                    txtISBNMissing.Visible = true;
                    lblISBNMissing.Visible = true;
                }
                else
                {
                    txtISBNMissing.Visible = false;
                    lblISBNMissing.Visible = false;
                }

                DataTable accountDataTable = DB.GetAccountDataTable(ISBNList,showTestData);
                gvAccountGrid.DataSource = accountDataTable;
                gvAccountGrid.DataBind();
            }
            else
            {
                ClearGridData(gvResultsGrid);
                ClearGridData((gvAccountGrid));
                SetBottomHalfVisibility(false);
            }
        }

        private void ShowISBNsNotFoundInDatabase(string ISBNList)
        {
            string[] isbnArray = ISBNList.Split(',');
            foreach (string isbn in isbnArray)
            {
                bool included = false;
                foreach (GridViewRow item in gvResultsGrid.Rows)
                {
                    if (item.Cells[2].Text == isbn)
                    {
                        included = true;
                        break;
                    }
                }

                if (included == false)
                {
                    WriteISBNChanges(isbn, txtISBNSearch, txtISBNMissing);
                }
            }
        }

        private string ProcessSingleISBN(string s)
        {
            string ISBN = "";
            if (ISBNValidator.ValidateISBN(s))
            {
                s = string.Format("'{0}'", s);
                ISBN = s;
            }
            else
            {
                SetBottomHalfVisibility(false);
                ClearAndHideTextBox(txtISBNMissing);
                lblISBNMissing.Visible = false;
                lblErrorLabel.Visible = true;
                lblErrorLabel.Text = "Please enter a valid ISBN, if you have entered a list of ISBNs it must be comma separated!";
                return ISBN = "Error";
            }

            return ISBN;
        }

        private string ProcessListOfISBNs(string s)
        {
            string ISBNList = "";
            s = RemoveFinalComma(s);
            string[] values = s.Split(',').Select(sValue => sValue.Trim()).ToArray();
            for (int i = 0; i < values.Length; i++)
            {
                if (ISBNValidator.ValidateISBN(values[i]))
                {
                    values[i] = string.Format("'{0}'", values[i]);
                }
                else
                {
                    SetBottomHalfVisibility(false);
                    txtISBNSearch.Text = "";
                    txtISBNMissing.Text = "";
                    txtISBNMissing.Visible = false;
                    lblISBNMissing.Visible = false;
                    lblErrorLabel.Visible = true;
                    lblErrorLabel.Text = "Please enter a valid ISBN!";
                    return ISBNList = "Error";
                }
            }

            ISBNList = string.Join(",", values);
            ISBNList = RemoveFinalComma(ISBNList);
            return ISBNList;
        }

        private string RemoveNewLinesAndTabs(string s)
        {
            s = s.Replace("\n", String.Empty);
            s = s.Replace("\r", String.Empty);
            s = s.Replace("\t", String.Empty);
            return s;
        }

        private string RemoveFinalComma(string s)
        {
            if (IsLastCharComma(s))
            {
                s = s.Remove(s.Length - 1, 1);
            }

            return s;
        }

        private bool IsLastCharComma(string s)
        {
            char last = s[s.Length - 1];
            return last == ',';
        }

        protected void btnEdit_OnClick(object sender, ImageClickEventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;
            string ISBN = gvr.Cells[2].Text;
            string fullFilePath = gvr.Cells[7].Text;
            string perspective = gvr.Cells[4].Text;

            if (perspective.ToLower() != "cover")
            {
                lblErrorLabel.Visible = true;
                lblErrorLabel.Text = "You cannot edit images that are not cover images!";
                return;
            }

            int divPos = 0;
            bool success = int.TryParse(Request.Form["div_position"], out divPos);
            if (success)
            {
                DB.ResultGridDivPosition = divPos;
            }

            //ResultGridContainer.Visible = false;
            
            SetBottomHalfVisibility(false);

            

            string login = GetLoginIDFromFilePath(fullFilePath);
            if (login == "") return;

            EditPanel.Visible = true;
            txtPanelISBN.Text = ISBN;
            string filePath = DB.GetFullFilePathForISBN(ISBN, login,perspective);
            txtPanelFilePath.Text = filePath;
            NewImage.ImageUrl = "images/NoImageSelected.png";
            
            if (!File.Exists(filePath))
            {
                CurrentImage.ImageUrl= "images/NoImageFound.png";
            }
            else
            {
                filePath = filePath.Replace("\\\\", "/");
                filePath = filePath.Replace("\\", "/");

                CurrentImage.ImageUrl = "ImageUtility.aspx?FilePath=" + filePath;
                CurrentImage.Width = 180;
                CurrentImage.Height = 260;
            }
            Page_Load(null, EventArgs.Empty);
        }

        protected void btnPreview_OnClick(object sender, EventArgs e)
        {
            string hiddenControlValue = Request["hdnfldVariable"];

            //string filePath = hdnfldVariable.Value.ToString();
            string filePath = hiddenControlValue;

            PreviewImage.ImageUrl = "ImageUtility.aspx?PreviewPath=" + @"E:\" + filePath;
            PreviewImage.Width = 180;
            PreviewImage.Height = 260;
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            string OriginalFilePath = CurrentImage.ImageUrl.Replace("ImageUtility.aspx?FilePath=", "");
            string FileToReplacePath = Server.MapPath(NewImage.ImageUrl) + FileUpload1.FileName;

            if (OriginalFilePath == "images/NoImageFound.png")
            {
                lblErrorLabel.Text = "No image found for this file path. There is nothing to replace!";
                lblErrorLabel.Visible = true;
                return;
            }

            if (!File.Exists(FileToReplacePath))
            {
                lblErrorLabel.Text = "No file or data was deleted. No file exists at : " + FileToReplacePath;
                lblErrorLabel.Visible = true;
                return;
            }

            string login = GetLoginIDFromFilePath(OriginalFilePath);
            if (login=="") return;
            
            if (File.Exists(OriginalFilePath))
            {
                System.Drawing.Image image1 = System.Drawing.Image.FromFile(OriginalFilePath);
                image1.Dispose();
                File.Delete(OriginalFilePath);  // error - file is locked 
            }

            System.Drawing.Image image2 = null;
            try
            {
                image2 = System.Drawing.Image.FromFile(FileToReplacePath);
                image2.Save(OriginalFilePath);

                int periodPosition = OriginalFilePath.LastIndexOf('.');
                string ThumbnailFile = OriginalFilePath.Insert(periodPosition, "_th");
                Size sizethumb = new Size(180, 180);
                System.Drawing.Image thumbnailImage = ImageHelper.Resize(image2, sizethumb);
                thumbnailImage.Save(ThumbnailFile);

                NewImage.ImageUrl = null;
                NewImage = null;
                lblSave.Visible = false;
                btnSave.Visible = false;
            }
            catch(Exception pathException)
            {
                image2.Dispose();
                lblErrorLabel.Visible = true;
                lblErrorLabel.Text = "No image was saved. Cannot find path: " + OriginalFilePath;
            }

            var fileLength = new FileInfo(OriginalFilePath).Length;

            ImageFileModel imageFileModel = new ImageFileModel();
            imageFileModel.FileProcessID = null;
            imageFileModel.LoginID = login;
            imageFileModel.EAN = txtPanelISBN.Text;

            string formattedPath = OriginalFilePath.Replace("/", "\\");
            imageFileModel.FullFilePath = OriginalFilePath;
            imageFileModel.Perspective = "cover";
            imageFileModel.FileSize = fileLength;
            imageFileModel.Width = image2.Width;
            imageFileModel.Height = image2.Height;
            imageFileModel.FileExtension = Path.GetExtension(OriginalFilePath);
            imageFileModel.FileCreated = DateTime.Now;
            imageFileModel.FileModified = DateTime.Now;
            imageFileModel.DateProcessed = DateTime.Now;
            imageFileModel.FileNumber = 0;
            imageFileModel.ISNI = "";

            image2.Dispose();

            try
            {
                DB.DeleteFromDB(imageFileModel.EAN, imageFileModel.LoginID, imageFileModel.Perspective);
                DB.InsertNewImageInformation(imageFileModel);
            }
            catch (Exception dbCallException)
            {
                lblErrorLabel.Visible = true;
                lblErrorLabel.Text = dbCallException.StackTrace;
                return;
            }

            LogItemModel logItem = new LogItemModel();
            logItem.LoginID = login;
            logItem.IncomingPath = OriginalFilePath;
            logItem.OutgoingPath = OriginalFilePath; // if not replaced this will be DELETED
            logItem.FileSize = fileLength;
            logItem.ProcessStateID = 4;
            logItem.FileTypeID = 1;
            logItem.DateUploaded = DateTime.Now;
            logItem.DateCompleted = DateTime.Now;
            logItem.TraceInfo = "ImageUtility - Replacing file at " + OriginalFilePath + " with file at " + FileToReplacePath;
            logItem.ErrorInfo = "";

            try
            {
                DB.LogChange(logItem);
            }
            catch (Exception dbLogException)
            {
                lblErrorLabel.Visible = true;
                lblErrorLabel.Text = dbLogException.StackTrace;
                return;
            }

        }

        protected void btnImageDelete_OnClick(object sender, EventArgs e)
        {
            string ISBN = txtPanelISBN.Text;
            string fullFilePath = txtPanelFilePath.Text;
            string login = GetLoginIDFromFilePath(fullFilePath);
            if (login == "") return;

            if (!File.Exists(fullFilePath))
            {
                lblErrorLabel.Visible = true;
                lblErrorLabel.Text = "No image found at: " + fullFilePath + ". There is no image to delete!";
                return;
            }

            try
            {
                DB.DeleteFromDB(ISBN, login, "cover");
            }
            catch (Exception dbCallException)
            {
                lblErrorLabel.Visible = true;
                lblErrorLabel.Text = "DELETE ERROR " + dbCallException.StackTrace;
                return;
            }


            //initialize this logging info before deleting the file
            var fileLength = new FileInfo(fullFilePath).Length;

            File.Delete(fullFilePath);

            //delete thumbnail image
            int periodPosition = fullFilePath.LastIndexOf('.');
            string ThumbnailFile = fullFilePath.Insert(periodPosition, "_th");
            File.Delete(ThumbnailFile);

            LogItemModel logItem = new LogItemModel();
            logItem.LoginID = login;
            logItem.IncomingPath = fullFilePath;
            logItem.OutgoingPath = "DELETED";
            logItem.FileSize = fileLength;
            logItem.ProcessStateID = 4;
            logItem.FileTypeID = 1;
            logItem.DateUploaded = DateTime.Now;
            logItem.DateCompleted = DateTime.Now;
            logItem.TraceInfo = "ImageUtility - Deleting file at " + fullFilePath;
            logItem.ErrorInfo = "";

            try
            {
                DB.LogChange(logItem);
            }
            catch (Exception dbCallException)
            {
                lblErrorLabel.Visible = true;
                lblErrorLabel.Text = "LOG ERROR " + dbCallException.StackTrace;
                return;
            }


            SetBottomHalfVisibility(true);
            EditPanel.Visible = false;

            WriteISBNChanges(ISBN, txtISBNSearch, txtISBNDeleted);
            lblISBNDeleted.Visible = true;
            txtISBNDeleted.Visible = true;

            CurrentImage.ImageUrl = "images/NoImageFound.png";
            NewImage.ImageUrl = "images/NoImageSelected.png;";
            PreviewImage.ImageUrl = "";
            LoadGrids(chkShowTestData.Checked);
        }

        private void WriteISBNChanges(string ISBN, TextBox from, TextBox to)
        {
            if (from.Text.Contains(ISBN + ",\r\n"))
            {
                from.Text = from.Text.Replace(ISBN + ",\r\n", "");
                to.Text = to.Text + ISBN + "," + Environment.NewLine;
            }
            else if (from.Text.Contains(ISBN + ","))
            {
                from.Text = from.Text.Replace(ISBN + ",", "");
                to.Text = to.Text + ISBN + "," + Environment.NewLine;
            }
            else if (from.Text.Contains(ISBN))
            {
                from.Text = from.Text.Replace(ISBN, "");
                to.Text = to.Text + ISBN + "," + Environment.NewLine;
            }
        }

        private void ClearAndHideTextBox(TextBox textBox)
        {
            textBox.Text = "";
            textBox.Visible = false;
        }

        protected void btnImageViewFolder_OnClick(object sender, EventArgs e)
        {
            string filePath = txtPanelFilePath.Text;
            //for testing on local C:\
            string subPath = "";
            int index = filePath.LastIndexOf("\\");
            if (index > 0)
            {
                subPath = filePath.Substring(0, index);
                subPath = subPath.Replace(@"\\", @"\");
            }
            else if (index == -1)
            {
                index = filePath.LastIndexOf("/");
                if (index > 0)
                {
                    subPath = filePath.Substring(0, index);
                    subPath = subPath.Replace(@"/", @"\");
                }
            }
            try
            {
                string folderPath = subPath.Remove(0, 3);
                var fileCount = (from file in Directory.EnumerateFiles(@subPath, "*", SearchOption.AllDirectories)
                    select file).Count();

                long numOfFiles = 0;
                numOfFiles = Convert.ToInt64(fileCount);

                if (numOfFiles > 20000)
                {
                    lblErrorLabel.Visible = true;
                    lblErrorLabel.Text =
                        "The view folder feature is disabled for folders that contain over 20000 files. You can still view the current image for this ISBN by using the Edit Button.";
                    return;
                }
                DB.ViewPaths[0] = @subPath;
                draggable.Visible = true;

                PreviewImage.ImageUrl = "ImageUtility.aspx?PreviewPath=" + filePath;
                PreviewImage.Width = 180;
                PreviewImage.Height = 260;
                Page_Load(null, EventArgs.Empty);
            }
            catch (Exception pathException)
            {
                lblErrorLabel.Visible = true;
                lblErrorLabel.Text = "Process cancelled, cannot find the path: " + subPath + "pathException: " + pathException.StackTrace;
            }
        }

        protected void chkShowTestData_OnCheckedChanged(object sender, EventArgs e)
        {
            LoadGrids(chkShowTestData.Checked);
        }
      

        private void ClearTempImageFiles()
        {
            try
            {
                CurrentImage.ImageUrl = "";
                NewImage.ImageUrl = "";
                string folderPath = Server.MapPath("~/Files/");

                //Check whether Directory (Folder) exists.
                if (Directory.Exists(folderPath))
                {
                    System.IO.DirectoryInfo di = new DirectoryInfo(folderPath);

                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }

                    foreach (DirectoryInfo dir in di.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                }
            }
            catch (Exception cacheException)
            {
                lblErrorLabel.Visible = true;
                lblErrorLabel.Text = cacheException + "- Image caching error! Please contact support.";
            }
            
        }
    }

}