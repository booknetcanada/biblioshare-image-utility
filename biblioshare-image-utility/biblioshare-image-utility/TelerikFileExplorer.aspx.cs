﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;


namespace ImageUtility
{
    public partial class TelerikFileExplorer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string[] viewPaths = new string[]
            {
                //@"C:\PhysicalSource\ROOT"
                //@"E:\BNCImages\00PUBTEST00"
                @"C:\BNCImages\00PUBTEST00"
            }; //@"\\Telerik.com\Path\SharedDir"
                                                                           //};
            string[] uploadPaths = new string[]
            {
               // @"C:\PhysicalSource\ROOT\CanUpload",
               // @"\\Telerik.com\Path\SharedDir\ROOT\CanUpload",
                //@"C:\PhysicalSource\ROOT\Folder_11\CanDelAndUpload"
               // @"\\Telerik.com\Path\SharedDir\ROOT\Folder_11\CanDelAndUpload"
               //@"E:\BNCImages\00PUBTEST00"
               //@"E:\BNCImages\00PUBTEST00"

            };
            string[] deletePaths = new string[]
            {
                //@"C:\PhysicalSource\ROOT\Folder_1\CanDelete",
                //@"C:\PhysicalSource\ROOT\Folder_11\CanDelAndUpload"
                //@"\\Telerik.com\Path\SharedDir\ROOT\Folder_1\CanDelete",
               // @"\\Telerik.com\Path\SharedDir\ROOT\Folder_11\CanDelAndUpload"
               //@"E:\BNCImages\00PUBTEST00"
              // @"E:\BNCImages\00PUBTEST00"

            };

            RadFileExplorer2.Configuration.ViewPaths = viewPaths;
            //RadFileExplorer2.Configuration.UploadPaths = uploadPaths;
            //RadFileExplorer2.Configuration.DeletePaths = deletePaths;
            RadFileExplorer2.Configuration.SearchPatterns = new[] { "*.*" };
            RadFileExplorer2.Configuration.ContentProviderTypeName = typeof(CustomFileSystemProvider).AssemblyQualifiedName;
        }
    }

}