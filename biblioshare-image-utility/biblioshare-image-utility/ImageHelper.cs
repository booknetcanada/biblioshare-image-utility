﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Web;

namespace ImageUtility
{
    public static class ImageHelper
    {
        public static Image Resize(Image img, Size NewSize)
        {
            Double ratio = 0.00;

            if (img.Width > img.Height)
            {
                ratio = (double)NewSize.Width / (double)img.Width;
            }
            else
            {
                ratio = (double)NewSize.Height / (double)img.Height;
            }

            Bitmap thumb = new Bitmap(Convert.ToInt32(img.Width * ratio), Convert.ToInt32(img.Height * ratio));

            using (Graphics g = Graphics.FromImage(thumb))
            {
                //if you want to tweak the quality of the drawing...
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                g.SmoothingMode = SmoothingMode.HighQuality;

                g.PixelOffsetMode = PixelOffsetMode.HighQuality;

                g.CompositingQuality = CompositingQuality.HighQuality;

                g.DrawImage(img, 0, 0, Convert.ToInt32(img.Width * ratio), Convert.ToInt32(img.Height * ratio));

                //BNLog.Write("system", "Created thumbnail from" + img.Width.ToString() + " x " + thumb.Height.ToString() + " to " + thumb.Width.ToString() + " x " + Convert.ToInt32(img.Height * ratio).ToString());
            }

            return thumb;
        }

    }
}