﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImageUtility
{
    public class ImageFileModel
    {
        public string FileProcessID { get; set; }
        public string LoginID { get; set; }
        public string EAN { get; set; }
        public string FullFilePath { get; set; }
        public string Perspective { get; set; }
        public long FileSize { get; set; }
        public long Width { get; set; }
        public long Height { get; set; }
        public string FileExtension { get; set; }
        public DateTime FileCreated { get; set; }
        public DateTime FileModified { get; set; }
        public DateTime DateProcessed { get; set; }
        public int FileNumber { get; set; }
        public string ISNI { get; set; }

    }
}