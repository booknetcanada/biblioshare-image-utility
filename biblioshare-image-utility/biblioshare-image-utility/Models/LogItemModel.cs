﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImageUtility
{
    public class LogItemModel
    {
        public string LoginID { get; set; }
        public string IncomingPath { get; set; }
        public string OutgoingPath { get; set; }
        public long FileSize { get; set; }
        public int ProcessStateID { get; set; }
        public int FileTypeID { get; set; }
        public DateTime DateUploaded { get; set; }
        public DateTime DateCompleted { get; set; }
        public string TraceInfo { get; set; }
        public string ErrorInfo { get; set; }
    }
}