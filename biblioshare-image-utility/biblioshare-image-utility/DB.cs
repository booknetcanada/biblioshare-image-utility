﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Windows.Forms;
using Telerik.Web.UI.FileExplorer;

namespace ImageUtility
{
    public static class DB
    {
        static string connStr = ConfigurationManager.ConnectionStrings["BiblioShare"].ConnectionString;
        static string loginStr = ConfigurationManager.ConnectionStrings["Login"].ConnectionString;
        public static string[] ViewPaths = { @"E:\BNCImages\" };
        public static int ResultGridDivPosition = 0;
        public static int GridPagingIndex = 0;
        public static Telerik.Web.UI.FileExplorer.FileExplorerMode ExplorerMode = FileExplorerMode.Thumbnails;
        public static DataTable GetResultsDataTable(string ISBN, bool showTestData)
        {
            DataTable dataTable = new DataTable();
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            SqlCommand command = conn.CreateCommand();
            command.CommandType = CommandType.Text;
            command.CommandTimeout = 600;
            if (showTestData)
            {
                command.CommandText = "select EAN,ISNULL(Suppliers.SupplierName,'Supplier Not Found') AS SupplierName,Perspective,DateProcessed,FileNumber,FullFilePath from[BNCFileServer].[dbo].[ImageFiles] left join Suppliers on Suppliers.SAN = ImageFiles.LoginID where EAN in (" + ISBN + ") order by Suppliers.SupplierName";
            }
            else
            {
                command.CommandText = "select EAN,ISNULL(Suppliers.SupplierName, 'Supplier Not Found') AS SupplierName,Perspective,DateProcessed,FileNumber,FullFilePath from[BNCFileServer].[dbo].[ImageFiles] left join Suppliers on Suppliers.SAN = ImageFiles.LoginID where EAN in (" + ISBN + ")"
                    + " and LoginID not in ('00PUBTEST00','115TOMTEST04','115TOMTEST10-prpr','BKNET_TEST','BKNET_TEST_SUPP','ImageTesting') order by Suppliers.SupplierName";
            }
            SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
            dataAdapter.Fill(dataTable);
            conn.Close();
            return dataTable;
        }

        public static DataTable GetAccountDataTable(string ISBN, bool showTestData)
        {
            DataTable accountDataTable = new DataTable();
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            if (showTestData)
            {
                cmd.CommandText = "select ISNULL(Suppliers.SupplierName, 'Supplier Not Found') AS SupplierName, (Count(ImageID)) as 'NumImage' from [BNCFileServer].[dbo].[ImageFiles] left join Suppliers on Suppliers.SAN = ImageFiles.LoginID where EAN in (" + ISBN + ") group by Suppliers.SupplierName";
            }
            else
            {
                cmd.CommandText = "select ISNULL(Suppliers.SupplierName, 'Supplier Not Found') AS SupplierName, (Count(ImageID)) as 'NumImage' from [BNCFileServer].[dbo].[ImageFiles] left join Suppliers on Suppliers.SAN = ImageFiles.LoginID where EAN in (" + ISBN + ")"
                                  + " and LoginID not in ('00PUBTEST00','115TOMTEST04','115TOMTEST10-prpr','BKNET_TEST','BKNET_TEST_SUPP','ImageTesting') group by Suppliers.SupplierName";
            }

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(accountDataTable);
            conn.Close();
            return accountDataTable;
        }

        public static void DeleteFromDB(string ISBN, string Login, string Perspective)
        {
            try
            {
                using (var sc = new SqlConnection(connStr))
                using (var cmd = sc.CreateCommand())
                {
                    sc.Open();
                    cmd.CommandText = "DELETE FROM [BNCFileServer].[dbo].[ImageFiles] WHERE EAN like '" + ISBN + "' and LoginID like '" + Login + "' and Perspective like '" + Perspective  + "'";
                    cmd.ExecuteNonQuery();
                    sc.Close();
                    sc.Dispose();
                }
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
        }

        public static void InsertNewImageInformation(ImageFileModel imageFileModel)
        {
            String query = "INSERT INTO [BNCFileServer].[dbo].[ImageFiles] (FileProcessID,LoginID,EAN,FullFilePath,Perspective,FileSize,Width,Height,FileExtension,FileCreated,FileModified,DateProcessed,FileNumber,ISNI)"
                           + " VALUES (@FileProcessID,@LoginID, @EAN, @FullFilePath,@Perspective,@FileSize,@Width,@Height,@FileExtension,@FileCreated,@FileModified,@DateProcessed,@FileNumber,@ISNI)";
            try
            {
                using (SqlConnection connection = new SqlConnection(connStr))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    //a shorter syntax to adding parameters
                    command.Parameters.AddWithValue("@FileProcessID", DBNull.Value);
                    command.Parameters.AddWithValue("@LoginID", imageFileModel.LoginID);
                    command.Parameters.AddWithValue("@EAN", imageFileModel.EAN);
                    command.Parameters.AddWithValue("@FullFilePath", imageFileModel.FullFilePath);
                    command.Parameters.AddWithValue("@Perspective", imageFileModel.Perspective);
                    command.Parameters.AddWithValue("@FileSize", imageFileModel.FileSize);
                    command.Parameters.AddWithValue("@Width", imageFileModel.Width);
                    command.Parameters.AddWithValue("@Height", imageFileModel.Height);
                    command.Parameters.AddWithValue("@FileExtension", imageFileModel.FileExtension);
                    command.Parameters.AddWithValue("@FileCreated", imageFileModel.FileCreated);
                    command.Parameters.AddWithValue("@FileModified", imageFileModel.FileModified);
                    command.Parameters.AddWithValue("@DateProcessed", imageFileModel.DateProcessed);
                    command.Parameters.AddWithValue("@FileNumber", imageFileModel.FileNumber);
                    if (!String.IsNullOrEmpty(imageFileModel.ISNI))
                    {
                        command.Parameters.AddWithValue("@ISNI", imageFileModel.ISNI);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@ISNI", DBNull.Value);
                    }

                    //make sure you open and close(after executing) the connection
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }


        }

        public static List<string> GetListOfFullFilePaths(string ISBNSToDelete, string Login)
        {
            List<string> filePathList = new List<string>();
            try
            {
                using (var sc = new SqlConnection(connStr))
                using (var cmd = sc.CreateCommand())
                {
                    sc.Open();
                    cmd.CommandText = "SELECT FullFilePath FROM [BNCFileServer].[dbo].[ImageFiles] WHERE EAN in (" + ISBNSToDelete + ") and LoginID like '" + Login + "'";
                    cmd.ExecuteNonQuery();

                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var filePathForISBN = dataReader.GetString(0);
                            filePathList.Add(filePathForISBN);
                        }
                    }
                    sc.Close();
                    sc.Dispose();
                }
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }

            return filePathList;
        }

        public static string GetFullFilePathForISBN(string ISBN, string Login, string perspective)
        {
            string ISBNReturned = "";
            try
            {
                using (var sc = new SqlConnection(connStr))
                using (var cmd = sc.CreateCommand())
                {
                    sc.Open();
                    cmd.CommandText = "SELECT FullFilePath FROM [BNCFileServer].[dbo].[ImageFiles] WHERE EAN like '" + ISBN + "' and LoginID like '" + Login + "'" + " and Perspective like '" + perspective + "'";
                    cmd.ExecuteNonQuery();

                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            ISBNReturned = dataReader.GetString(0);
                        }
                    }
                    sc.Close();
                    sc.Dispose();
                }
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }

            return ISBNReturned;
        }

        public static void LogChange(LogItemModel logItem)
        {
            try
            {
                using (var connection = new SqlConnection(connStr))
                {
                    connection.Open();
                    var sql = "INSERT INTO [LogIncomingFile](LoginID, IncomingPath, OutgoingPath, FileSize, ProcessStateID, FileTypeID, DateUploaded, DateCompleted, TraceInfo, ErrorInfo)" +
                        " VALUES(@LoginID, @IncomingPath, @OutgoingPath, @FileSize, @ProcessStateID, @FileTypeID, @DateUploaded, @DateCompleted, @TraceInfo, @ErrorInfo)";
                    using (var cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@LoginID", logItem.LoginID);
                        cmd.Parameters.AddWithValue("@IncomingPath", logItem.IncomingPath);
                        cmd.Parameters.AddWithValue("@OutgoingPath", logItem.OutgoingPath);
                        cmd.Parameters.AddWithValue("@FileSize", logItem.FileSize);
                        cmd.Parameters.AddWithValue("@ProcessStateID", logItem.ProcessStateID);
                        cmd.Parameters.AddWithValue("@FileTypeID", logItem.FileTypeID);
                        cmd.Parameters.AddWithValue("@DateUploaded", logItem.DateUploaded);
                        cmd.Parameters.AddWithValue("@DateCompleted", logItem.DateCompleted);
                        cmd.Parameters.AddWithValue("@TraceInfo", logItem.TraceInfo);
                        cmd.Parameters.AddWithValue("@ErrorInfo", logItem.ErrorInfo);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }



        public static bool Login(string email, string password)
        {
            SqlConnection con = new SqlConnection(loginStr); // making connection   
            SqlDataAdapter sda = new SqlDataAdapter("SELECT COUNT(*) FROM [H12-BIBLIOSHARE].Biblioshare.dbo.tblusers WHERE (Email='" + email + "' or Username= '" + email +  "') AND Password='" + password + "'", con);
            /* in above line the program is selecting the whole data from table and the matching it with the user name and password provided by user. */
            DataTable dt = new DataTable(); //this is creating a virtual table  
            sda.Fill(dt);
            return dt.Rows[0][0].ToString() == "1";

        }

    }
}