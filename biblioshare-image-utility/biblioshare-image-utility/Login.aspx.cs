﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace ImageUtility
{
    public partial class Login : System.Web.UI.Page
    { 
        protected void btnLogin_OnClick(object sender, EventArgs e)
        {
            Authenticated.LoggedIn = DB.Login(txtEmailAddress.Text, txtPassword.Text);
            if (Authenticated.LoggedIn)
            {
                Response.RedirectPermanent("ImageUtility.aspx");
            }
            else
            {
                lblError.Text = "Invalid email address or password.";
                lblError.Visible = true;
            }
        }
    }
}